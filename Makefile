PROJECT = $(notdir $(CURDIR))
COMPILER = arm-none-eabi-

CC		= $(COMPILER)gcc
CP		= $(COMPILER)objcopy
AS		= $(COMPILER)as
LD 		= $(COMPILER)ld
OD		= $(COMPILER)objdump
SIZE    = $(COMPILER)size

DEF+=USE_FULL_ASSERT
DEF+=USE_STDPERIPH_DRIVER
DEF+=STM32F429_439xx
DEF+=HSE_VALUE=8000000
DEF+=___FPU_USED=1
DEF+=___FPU_PRESENT=1

ifeq ($(HARDFP),1)
	FLOAT_ABI = hard
else
	FLOAT_ABI = softfp
endif
CPU = -mthumb -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=softfp 

STM32F4xx_StdPeriph_Driver 	= 	STM32F4xx_StdPeriph_Driver
CMSIS						=   CMSIS
STM32F4xx 					= 	$(CMSIS)/Device/ST/STM32F4xx


VPATH+=: .
VPATH+=: $(STM32F4xx_StdPeriph_Driver)/src
VPATH+=: $(STM32F4xx)/Source:
VPATH+=: CMSIS/Device/ST/STM32F4xx/Source/Templates/TrueSTUDIO
VPATH+=: CMSIS/Device/ST/STM32F4xx/Source/Templates
VPATH+=: GCC-ARM
VPATH+=: driver
VPATH+=: FreeRTOS


INC+= $(STM32F4xx_StdPeriph_Driver)/inc
INC+= $(STM32F4xx)/Include 
INC+= $(CMSIS)/Include
INC+= .
INC+= driver/include
INC+= FreeRTOS/portable/GCC/ARM_CM4F
INC+= FreeRTOS/include



SRC+= FreeRTOS/portable/GCC/ARM_CM4F/port.c
SRC+= FreeRTOS/portable/MemMang/heap_4.c
SRC+= tasks.c
SRC+= queue.c
SRC+= list.c
SRC+= timers.c
SRC+= main.c
SRC+= system_stm32f4xx.c
SRC+= hal_com.c
SRC+= hal_irq.c
SRC+= stm32f4xx_dma.c
SRC+= misc.c
SRC+= stm32f4xx_usart.c
SRC+= stm32f4xx_rcc.c
SRC+= stm32f4xx_gpio.c
SRC+= rbuffer.c



STARTUP= startup_stm32f429_439xx.s



LDSCRIPT= GCC-ARM/STM32F439NI_FLASH.ld



OUTPUT 	= GCC-ARM/Output
DEPDIR 	= GCC-ARM/Dependencies

$(shell mkdir -p $(OUTPUT))
$(shell mkdir -p $(DEPDIR))




OBJS+=$(patsubst %.c,%.o,$(SRC))
OBJS+=$(patsubst %.s,%.o,$(STARTUP))
OBJECTS = $(addprefix $(OUTPUT)/, $(OBJS))

CFLAGS+=$(patsubst %,-I%,$(INC))
CFLAGS+=$(patsubst %,-D%,$(DEF))
CFLAGS+=-g
CFLAGS+=$(CPU)
CFLAGS+=-Wp,-MM,-MP,-MT,$(OUTPUT)/$(*F).o,-MF,$(DEPDIR)/$(*F).d

LDFLAGS+=$(patsubst %,-T%,$(LDSCRIPT))
LDFLAGS+=$(CPU) -Wl,--gc-sections --specs=nano.specs -u _printf_float -u _scanf_float  -Wl,-Map=$(PROJECT).map,--cref

LDFLAGS+=$(CFLAGS)
#LDFLAGS+=-lrdimon
#LDFLAGS+=--specs=rdimon.specs
#Don't use semihosting
LDFLAGS+=--specs=nosys.specs
#use newlib nano
#LDFLAGS+=--specs=nano.specs 
LDFLAGS+=-lnosys
LDFLAGS+=-lgcc
LDFLAGS+=-lm
LDFLAGS+=-lc
LDFLAGS+=-lg

ASFLAGS+=-g
ASFLAGS+=$(CPU)

$(OUTPUT)/%.o: %.s Makefile
	@mkdir -p $(@D)
	@$(AS) $(ASFLAGS) -mthumb $< -o $@
	@echo "AS ${@}"
$(OUTPUT)/%.o: %.c Makefile $(INC)
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) -std=gnu99 -c $< -o $@
	@echo "CC ${@}"



all: $(PROJECT).elf $(PROJECT).bin size

$(PROJECT).elf : $(OBJECTS)
	@mkdir -p $(@D)
	@$(CC) $^ $(LDFLAGS) $(LIBS) -o $@
	@echo "LD ${@}"
$(PROJECT).bin : $(PROJECT).elf
	@mkdir -p $(@D)
	@$(CP) -O binary $(PROJECT).elf $(PROJECT).bin
	@echo "CP ${@}"

size: $(PROJECT).elf
	@$(SIZE) $(PROJECT).elf
	
clean: 
	@rm -f $(OBJECTS) $(PROJECT).bin $(DEPDIR)/*.d $(PROJECT).elf $(PROJECT).map
flash:
	@st-flash --reset write $(PROJECT).bin 0x8000000

-include $(wildcard $(DEPDIR)/*.d)
