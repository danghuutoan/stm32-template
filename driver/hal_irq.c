/*******************************************************************************
 * Copyright (c) 2015, The LightCo
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are strictly prohibited without prior permission of The LightCo.
 *
 * @file    hal_irq.c
 * @author  The LightCo
 * @version V1.0.0
 * @date    Sept-09-2015
 * @brief   This file contains definitions of the ...
 *
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "hal_irq.h"
#include "misc.h"
/* Privated variables ------------------------------------------------------- */

/* Hardware Sub priority */
static uint8_t hw_preem_priority_index = 0;
/* Exported functions ------------------------------------------------------- */

/**
 * @brief hal_irq_init
 * The function shall ...
 * @param 
 * @return 
 */
void hal_irq_init( void )
{
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
}

/**
 * @brief hal_irq_init
 * The function shall ...
 * @param 
 * @return 
 */
uint8_t hal_irq_get_preem_priority( void )
{
    return hw_preem_priority_index++;
}

/**
 * @brief hal_irq_init
 * The function shall ...
 * @param 
 * @return 
 */
uint8_t hal_irq_get_sub_priority ( void )
{
    return 0;
}

/*********** Portions COPYRIGHT 2015 Light.Co., Ltd.*****END OF FILE****/
