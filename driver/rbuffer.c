#include "rbuffer.h"
#include  <string.h>

#define MODULO_INC(a,b)		 ((a+1)&(b-1))

void 			ring_buffer_flush	( ring_buffer_t* r_buf ) {

	if(r_buf->buffer) {
		memset (r_buf->buffer, 0, sizeof (r_buf->buffer));
	} /* if buffer valid */
}

r_buff_error_t 	ring_buffer_status	( ring_buffer_t* r_buf ) {

	if(r_buf->count >= R_BUFFER_SIZE ){
		return R_BUF_FULL;
	} /* if empty */
	else if(r_buf->count == 0 ){
		return R_BUF_EMPTY;
	} /* if full */
	else {
		return R_BUF_READY;
	} /* if ready */
}

void 			ring_buffer_init	( ring_buffer_t* r_buf ){

	r_buf->count = 0;
	r_buf->head  = 0;
	r_buf->tail  = 0;
	ring_buffer_flush(r_buf);
}

r_buff_error_t 	ring_buffer_put	    ( ring_buffer_t* r_buf, uint8_t  data ){
	
	r_buff_error_t buffer_status = ring_buffer_status(r_buf);
	if( buffer_status == R_BUF_FULL ){
		return R_BUF_FULL;
	}
	else {
		r_buf->buffer[r_buf->head] = data;
		r_buf->head = MODULO_INC(r_buf->head, R_BUFFER_SIZE);
		r_buf->count++;
	}
	return R_BUF_OK;
}

r_buff_error_t 	ring_buffer_get	    ( ring_buffer_t* r_buf, uint8_t *data ){	
	
	r_buff_error_t buffer_status = ring_buffer_status(r_buf);
	if( buffer_status == R_BUF_EMPTY ){
		return R_BUF_EMPTY;
	}
	else {
		*data = r_buf->buffer[ r_buf->tail ];
		r_buf->tail = MODULO_INC(r_buf->tail, R_BUFFER_SIZE);
		r_buf->count--;
	}
	return R_BUF_OK;
}
