/*******************************************************************************
 * Copyright (c) 2015, The LightCo
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are strictly prohibited without prior permission of The LightCo.
 *
 * @file    hal_irq.h
 * @author  The LightCo
 * @version V1.0.0
 * @date    Sept-09-2015
 * @brief   This file contains definitions of the ...
 *
 ******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HAL_IRQ_H__
#define __HAL_IRQ_H__

#ifdef __cplusplus
 extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "std_type.h"

/* Exported functions ------------------------------------------------------- */
/**
 * @brief hal_irq_init
 * The function shall ...
 * @param 
 * @return 
 */
void hal_irq_init( void );

/**
 * @brief hal_irq_get_preem_priority
 * The function shall ...
 * @param 
 * @return 
 */
uint8_t hal_irq_get_preem_priority( void );

/**
 * @brief hal_irq_get_sub_priority
 * The function shall ...
 * @param 
 * @return 
 */
uint8_t hal_irq_get_sub_priority ( void );

#ifdef __cplusplus
 }
#endif
#endif /* __HAL_IRQ_H__ */

/*********** Portions COPYRIGHT 2015 Light.Co., Ltd.*****END OF FILE****/
