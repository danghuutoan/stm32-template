#ifndef __R_BUFFER_H__
#define __R_BUFFER_H__

#include <stdint.h>

/* this number must be power of 2 */
#define R_BUFFER_SIZE 	256

typedef enum {
	R_BUF_READY,
	R_BUF_EMPTY,
	R_BUF_FULL,
	R_BUF_OK
} r_buff_error_t;

typedef struct ring_buffer_st {
	uint16_t  count;
	uint16_t  head;
	uint16_t  tail;
	uint8_t   buffer[R_BUFFER_SIZE];
} ring_buffer_t;

void 			ring_buffer_flush	( ring_buffer_t* r_buf );
void 			ring_buffer_init	( ring_buffer_t* r_buf );
r_buff_error_t 	ring_buffer_status	( ring_buffer_t* r_buf );
r_buff_error_t 	ring_buffer_put	    ( ring_buffer_t* r_buf, uint8_t  data );
r_buff_error_t 	ring_buffer_get	    ( ring_buffer_t* r_buf, uint8_t *data );

#endif /* __R_BUFFER_H__ */
