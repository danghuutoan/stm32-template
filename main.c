
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "hal_com.h"
#include "rbuffer.h"
#include <stdio.h>
#define ON                      0x01
#define OFF                     0x00

static uint8_t   c;
static volatile uint8_t   rc = 0;
ring_buffer_t com_buffer;
xQueueHandle serialQueue;
xSemaphoreHandle xMutex;
typedef struct msg_st {
  uint8_t message[100];
  uint8_t len;
} msg_t;
/*
 * com_irq_handler
 */
static void com_irq_handler(uint8_t _c)
{
    c = _c;
    rc = 1;
}

static hal_com_t com_dbg =
{
    .port_name       = COM3,
    .baudrate        = COM_BAUD_115200,
    .priority        = COM_PRIO_4,
    .irq_handler     = com_irq_handler,
    .data            = &c,
    .dma_en          = ON,
    .dma_tx_completed = 0
} ;

int _read(int file, char *ptr, int len)
{
    int CharCnt = 0x00;
    while(CharCnt < len)
    {
        *ptr++ = hal_com_readbyte(&com_dbg);
        CharCnt++;
    }
    return CharCnt;
}

/*
 * _write
 */
int _write(int file, char *ptr, int len)
{
    int n;
    msg_t printbuffer;
    printbuffer.len     = len;
    for(n = 0; n<len ; n++)
      {
        printbuffer.message[n] = *ptr;
        ptr++;
      }
    xQueueSend(serialQueue,&printbuffer,100);

    return n;
}
static void vTask1( void *pvParameters );
static void vTask2( void *pvParameters );
static void vTask3( void *pvParameters );
static void vTask4( void *pvParameters );
int main (void)
{
  hal_com_init(&com_dbg);
  hal_com_sendblock(&com_dbg,"HELLO",5);
  while (!com_dbg.dma_tx_completed);
  ring_buffer_init(&com_buffer);
  xMutex = xSemaphoreCreateMutex();
  serialQueue = xQueueCreate(8,sizeof(msg_t ));
  xTaskCreate( vTask1, "Task1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
  xTaskCreate( vTask2, "Task2", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
  xTaskCreate( vTask3, "Task3", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
  xTaskCreate( vTask4, "Task4", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
  vTaskStartScheduler();
  /* Will only get here if there was not enough heap space to create the
  idle task. */
  return 0;
}

void vTask1( void *pvParameters ) {
    for( ;; ) {
        /* test message */
        printf("task11111111111111111111111111111111111111111\r\n");
        vTaskDelay(1);
    }
}
void vTask3( void *pvParameters ) {
    for( ;; ) {
        /* test message */
        printf("taskhh33333333333333333333333333333333333333\r\n");
        vTaskDelay(1);
    }
}

void vTask4( void *pvParameters ) {
    for( ;; ) {
        /* test message */
        printf("task4444444444444444444444444444444444444\r\n");
        vTaskDelay(1);
    }
}
msg_t receivebuffer;
void vTask2( void *pvParameters ) {
    portBASE_TYPE xStatus;
    for( ;; ) {
      xStatus = xQueueReceive(serialQueue,&receivebuffer,0);
      if(xStatus == pdPASS){
          hal_com_sendblock(&com_dbg,receivebuffer.message,receivebuffer.len);
         while (!com_dbg.dma_tx_completed);
      } /* if data is received */
    }
}
/**
  * @brief  The assert_param macro is used for function's parameters check.
  * @param  expr: If expr is false, it calls assert_failed function
  *   which reports the name of the source file and the source
  *   line number of the call that failed.
  *   If expr is true, it returns no value.
  * @retval None
  */
  #define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
/* Exported functions ------------------------------------------------------- */
  void assert_failed(uint8_t* file, uint32_t line)
  {


  }

  void vApplicationTickHook( void )
  {

  }
  /*-----------------------------------------------------------*/

  void vApplicationMallocFailedHook( void )
  {
      /* vApplicationMallocFailedHook() will only be called if
      configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
      function that will get called if a call to pvPortMalloc() fails.
      pvPortMalloc() is called internally by the kernel whenever a task, queue,
      timer or semaphore is created.  It is also called by various parts of the
      demo application.  If heap_1.c or heap_2.c are used, then the size of the
      heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
      FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
      to query the size of free heap space that remains (although it does not
      provide information on how the remaining heap might be fragmented). */
      taskDISABLE_INTERRUPTS();
      for( ;; );
  }
  /*-----------------------------------------------------------*/

  void vApplicationIdleHook( void )
  {
      /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
      to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
      task.  It is essential that code added to this hook function never attempts
      to block in any way (for example, call xQueueReceive() with a block time
      specified, or call vTaskDelay()).  If the application makes use of the
      vTaskDelete() API function (as this demo application does) then it is also
      important that vApplicationIdleHook() is permitted to return to its calling
      function, because it is the responsibility of the idle task to clean up
      memory allocated by the kernel to any task that has since been deleted. */
  }
  /*-----------------------------------------------------------*/

  void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
  {
      ( void ) pcTaskName;
      ( void ) pxTask;

      /* Run time stack overflow checking is performed if
      configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
      function is called if a stack overflow is detected. */
      taskDISABLE_INTERRUPTS();
      for( ;; );
  }
  /*-----------------------------------------------------------*/
